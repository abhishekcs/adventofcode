use std::io::{self, Read, Write};

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;

    part1(&input)?;
    part2(&input)?;
    Ok(())
}

fn part1(input: &str) -> Result<()> {
    let adapters: std::result::Result<Vec<u64>, _> =
        input.lines().map(|line| line.parse()).collect();

    let mut adapters: Vec<u64> = adapters?;
    adapters.as_mut_slice().sort();

    let mut diff1 = 0;
    let mut _diff2 = 0;
    let mut diff3 = 0;
    let mut prev = 0;

    for joltage in adapters {
        match joltage - prev {
            1 => diff1 += 1,
            2 => _diff2 += 1,
            3 => diff3 += 1,
            _ => panic!("IMPOSSIBLE"),
        }

        prev = joltage;
    }

    diff3 += 1;

    writeln!(io::stdout(), "{}", diff1 * diff3)?;

    Ok(())
}

fn part2(input: &str) -> Result<()> {
    let adapters: std::result::Result<Vec<u64>, _> =
        input.lines().map(|line| line.parse()).collect();

    let mut adapters: Vec<u64> = adapters?;
    adapters.as_mut_slice().sort();

    let mut arrangements: Vec<u64> = vec![0; adapters.len()];
    for i in 0..adapters.len() {
        // Can the current adapter be a starting point
        if adapters[i] - 0 <= 3 {
            arrangements[i] += 1;
        }

        // Can it be added on to arrangements of i - 1 adapter
        if i > 0 && adapters[i] - adapters[i - 1] <= 3 {
            arrangements[i] += arrangements[i - 1]
        }

        // Can it be added on to arrangements i - 2 adapter
        if i > 1 && adapters[i] - adapters[i - 2] <= 3 {
            arrangements[i] += arrangements[i - 2]
        }

        // Can it be added on to arrangements of i - 3 adapter
        if i > 2 && adapters[i] - adapters[i - 3] <= 3 {
            arrangements[i] += arrangements[i - 3]
        }
    }

    writeln!(io::stdout(), "{}", arrangements[arrangements.len() - 1])?;

    Ok(())
}
