use std::io::{self, Read, Write};

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;

    part1(&input)?;
    part2(&input)?;
    Ok(())
}

fn part1(input: &str) -> Result<()> {
    let mut layout = parse_layout(input);
    let mut next_layout = next_1(&layout);
    while layout != next_layout {
        layout = next_layout;
        next_layout = next_1(&layout);
    }

    let total_occupied: usize = layout
        .into_iter()
        .map(|row| row.into_iter().filter(|c| *c == '#').count())
        .sum();

    writeln!(io::stdout(), "{}", total_occupied)?;

    Ok(())
}

fn part2(input: &str) -> Result<()> {
    let mut layout = parse_layout(input);
    let mut next_layout = next_2(&layout);
    while layout != next_layout {
        layout = next_layout;
        next_layout = next_2(&layout);
    }

    let total_occupied: usize = layout
        .into_iter()
        .map(|row| row.into_iter().filter(|&c| c == '#').count())
        .sum();

    writeln!(io::stdout(), "{}", total_occupied)?;

    Ok(())
}

fn parse_layout(layout: &str) -> Vec<Vec<char>> {
    layout.lines().map(|line| line.chars().collect()).collect()
}

fn next_1(layout: &Vec<Vec<char>>) -> Vec<Vec<char>> {
    let mut next_layout = layout.clone();
    for (i, row) in layout.iter().enumerate() {
        for (j, _) in row.iter().enumerate() {
            if becomes_occupied_1(layout, i, j) {
                next_layout[i][j] = '#'
            } else if becomes_empty_1(layout, i, j) {
                next_layout[i][j] = 'L'
            }
        }
    }

    next_layout
}

fn next_2(layout: &Vec<Vec<char>>) -> Vec<Vec<char>> {
    let mut next_layout = layout.clone();
    for (i, row) in layout.iter().enumerate() {
        for (j, _) in row.iter().enumerate() {
            if becomes_occupied_2(layout, i, j) {
                next_layout[i][j] = '#'
            } else if becomes_empty_2(layout, i, j) {
                next_layout[i][j] = 'L'
            }
        }
    }

    next_layout
}

fn is_occupied(layout: &Vec<Vec<char>>, row: usize, col: usize) -> bool {
    layout[row][col] == '#'
}

fn is_empty(layout: &Vec<Vec<char>>, row: usize, col: usize) -> bool {
    layout[row][col] == 'L'
}

fn becomes_occupied_1(layout: &Vec<Vec<char>>, row: usize, col: usize) -> bool {
    is_empty(layout, row, col) && count_occupied_adj(layout, row, col) == 0
}

fn becomes_empty_1(layout: &Vec<Vec<char>>, row: usize, col: usize) -> bool {
    is_occupied(layout, row, col) && count_occupied_adj(layout, row, col) >= 4
}

fn becomes_occupied_2(layout: &Vec<Vec<char>>, row: usize, col: usize) -> bool {
    is_empty(layout, row, col) && count_occupied_los(layout, row, col) == 0
}

fn becomes_empty_2(layout: &Vec<Vec<char>>, row: usize, col: usize) -> bool {
    is_occupied(layout, row, col) && count_occupied_los(layout, row, col) >= 5
}

enum Direction {
    Up,
    UpRight,
    Right,
    DownRight,
    Down,
    DownLeft,
    Left,
    UpLeft,
}

fn count_occupied_los(layout: &Vec<Vec<char>>, row: usize, col: usize) -> u64 {
    let mut occupied = 0;

    if is_occupied_los(layout, row, col, Direction::Up) {
        occupied += 1;
    }

    if is_occupied_los(layout, row, col, Direction::UpRight) {
        occupied += 1;
    }

    if is_occupied_los(layout, row, col, Direction::Right) {
        occupied += 1;
    }

    if is_occupied_los(layout, row, col, Direction::DownRight) {
        occupied += 1;
    }

    if is_occupied_los(layout, row, col, Direction::Down) {
        occupied += 1;
    }

    if is_occupied_los(layout, row, col, Direction::DownLeft) {
        occupied += 1;
    }

    if is_occupied_los(layout, row, col, Direction::Left) {
        occupied += 1;
    }

    if is_occupied_los(layout, row, col, Direction::UpLeft) {
        occupied += 1;
    }

    occupied
}

fn is_occupied_los(layout: &Vec<Vec<char>>, row: usize, col: usize, direction: Direction) -> bool {
    let (row_delta, col_delta) = match direction {
        Direction::Up => (-1, 0),
        Direction::UpRight => (-1, 1),
        Direction::Right => (0, 1),
        Direction::DownRight => (1, 1),
        Direction::Down => (1, 0),
        Direction::DownLeft => (1, -1),
        Direction::Left => (0, -1),
        Direction::UpLeft => (-1, -1),
    };

    let mut row = (row as isize) + row_delta;
    let mut col = (col as isize) + col_delta;
    while row >= 0
        && row < (layout.len() as isize)
        && col >= 0
        && col < (layout[row as usize].len() as isize)
    {
        if is_occupied(layout, row as usize, col as usize) {
            return true;
        } else if is_empty(layout, row as usize, col as usize) {
            return false;
        }

        row = row + row_delta;
        col = col + col_delta;
    }

    false
}

fn count_occupied_adj(layout: &Vec<Vec<char>>, row: usize, col: usize) -> u64 {
    let up_ok = row > 0;
    let right_ok = col < layout[row].len() - 1;
    let down_ok = row < layout.len() - 1;
    let left_ok = col > 0;

    let mut occupied = 0;
    // up
    if up_ok && is_occupied(layout, row - 1, col) {
        occupied += 1;
    }

    // up_right
    if up_ok && right_ok && is_occupied(layout, row - 1, col + 1) {
        occupied += 1;
    }

    // right
    if right_ok && is_occupied(layout, row, col + 1) {
        occupied += 1;
    }

    // down_right
    if down_ok && right_ok && is_occupied(layout, row + 1, col + 1) {
        occupied += 1;
    }

    // down
    if down_ok && is_occupied(layout, row + 1, col) {
        occupied += 1;
    }

    // down_left
    if down_ok && left_ok && is_occupied(layout, row + 1, col - 1) {
        occupied += 1;
    }

    // left
    if left_ok && is_occupied(layout, row, col - 1) {
        occupied += 1;
    }

    // up_left
    if up_ok && left_ok && is_occupied(layout, row - 1, col - 1) {
        occupied += 1;
    }

    occupied
}
