use std::io::{self, Read, Write};

type Result<T> = core::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;

    part1(&input)?;
    part2(&input)?;

    Ok(())
}

fn part1(input: &str) -> Result<()> {
    let max_seat_id = input.lines().map(seat_id).max().unwrap();

    writeln!(io::stdout(), "{}", max_seat_id)?;
    Ok(())
}

fn part2(input: &str) -> Result<()> {
    let mut seats: Vec<u64> = input.lines().map(seat_id).collect();
    seats.sort();
    for i in 1..seats.len() {
        if seats[i] != seats[i - 1] + 1 {
            writeln!(io::stdout(), "{}", seats[i - 1] + 1)?;
            return Ok(());
        }

        if seats[i] != seats[i + 1] - 1 {
            writeln!(io::stdout(), "{}", seats[i + 1] - 1)?;
            return Ok(());
        }
    }

    Ok(())
}

fn seat_id(id: &str) -> u64 {
    let (row, col) = id.split_at(7);
    let row = binary_partition(row, 'B', 'F');
    let col = binary_partition(col, 'R', 'L');
    row * 8 + col
}

fn binary_partition(sequence: &str, _up: char, down: char) -> u64 {
    let (_, val) =
        sequence
            .chars()
            .fold((0, 2u64.pow(sequence.len() as u32) - 1), |(low, up), c| {
                let mid = (low + up) / 2;
                if c == down {
                    (low, mid)
                } else {
                    (mid + 1, up)
                }
            });

    val
}
