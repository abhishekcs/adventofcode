use std::io::{self, Read, Write};
use std::str::FromStr;
use thiserror::Error;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;

    let instructions = parse_instructions(&input)?;

    writeln!(io::stdout(), "{}", part1(&instructions))?;
    writeln!(io::stdout(), "{}", part2(&instructions))?;
    Ok(())
}

#[derive(Debug, Clone, Copy)]
pub enum Instruction {
    N(i64),
    S(i64),
    E(i64),
    W(i64),
    L(i64),
    R(i64),
    F(i64),
}

#[derive(Debug, Error)]
pub enum ParseInstructionError {
    #[error("invalid instruction length: {0}")]
    InvalidLength(String),

    #[error("invalid instruction type: {0}")]
    InvalidType(String),

    #[error("invalid instruction value: {0}")]
    InvalidValue(#[from] std::num::ParseIntError),
}

impl FromStr for Instruction {
    type Err = ParseInstructionError;

    fn from_str(instruction: &str) -> std::result::Result<Self, Self::Err> {
        if instruction.len() < 2 {
            return Err(Self::Err::InvalidLength(instruction.to_string()));
        }
        let value: i64 = instruction[1..].parse()?;
        let itype = instruction.chars().next().unwrap(); // unwrap is safe since length has been checked

        match itype {
            'N' => Ok(Self::N(value)),
            'S' => Ok(Self::S(value)),
            'E' => Ok(Self::E(value)),
            'W' => Ok(Self::W(value)),
            'L' => Ok(Self::L(value)),
            'R' => Ok(Self::R(value)),
            'F' => Ok(Self::F(value)),
            _ => Err(Self::Err::InvalidType(instruction.to_string())),
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Direction {
    North,
    South,
    East,
    West,
}

impl Direction {
    pub fn rotate_clockwise(self, by: i64) -> Direction {
        let directions = [Self::North, Self::East, Self::South, Self::West];
        let times = by / 90;

        let initial = match self {
            Self::North => 0,
            Self::East => 1,
            Self::South => 2,
            Self::West => 3,
        };

        directions[((initial + times) % directions.len() as i64) as usize]
    }

    pub fn rotate_anticlockwise(self, by: i64) -> Direction {
        let directions = [Self::North, Self::West, Self::South, Self::East];
        let times = by / 90;

        let initial = match self {
            Self::North => 0,
            Self::East => 3,
            Self::South => 2,
            Self::West => 1,
        };

        directions[((initial + times) % directions.len() as i64) as usize]
    }
}

#[derive(Debug)]
pub struct Ship1 {
    direction: Direction,
    x: i64,
    y: i64,
}

impl Ship1 {
    pub fn distance(&self) -> u64 {
        self.x.abs() as u64 + self.y.abs() as u64
    }

    pub fn navigate(&mut self, instruction: Instruction) {
        match instruction {
            Instruction::N(by) => self.move_direction(Direction::North, by),
            Instruction::S(by) => self.move_direction(Direction::South, by),
            Instruction::E(by) => self.move_direction(Direction::East, by),
            Instruction::W(by) => self.move_direction(Direction::West, by),
            Instruction::L(by) => self.direction = self.direction.rotate_anticlockwise(by),
            Instruction::R(by) => self.direction = self.direction.rotate_clockwise(by),
            Instruction::F(by) => self.move_direction(self.direction, by),
        }
    }

    fn move_direction(&mut self, direction: Direction, by: i64) {
        match direction {
            Direction::North => self.y += by,
            Direction::South => self.y -= by,
            Direction::East => self.x += by,
            Direction::West => self.x -= by,
        }
    }
}

impl Default for Ship1 {
    fn default() -> Self {
        Self {
            direction: Direction::East,
            x: 0,
            y: 0,
        }
    }
}

#[derive(Debug)]
pub struct Waypoint {
    x: i64,
    y: i64,
}

impl Waypoint {
    pub fn rotate_clockwise(&mut self, by: i64) {
        let times = (by / 90) % 4;

        if times == 1 {
            let temp = self.y;
            self.y = -self.x;
            self.x = temp;
        } else if times == 2 {
            self.x = -self.x;
            self.y = -self.y;
        } else if times == 3 {
            self.rotate_clockwise(180);
            self.rotate_clockwise(90);
        }
    }

    pub fn rotate_anticlockwise(&mut self, by: i64) {
        let times = (by / 90) % 4;

        if times == 1 {
            let temp = self.x;
            self.x = -self.y;
            self.y = temp;
        } else if times == 2 {
            self.x = -self.x;
            self.y = -self.y;
        } else if times == 3 {
            self.rotate_anticlockwise(180);
            self.rotate_anticlockwise(90);
        }
    }
}

#[derive(Debug)]
pub struct Ship2 {
    x: i64,
    y: i64,
    waypoint: Waypoint,
}

impl Default for Ship2 {
    fn default() -> Self {
        Self {
            x: 0,
            y: 0,
            waypoint: Waypoint { x: 10, y: 1 },
        }
    }
}

impl Ship2 {
    pub fn distance(&self) -> u64 {
        self.x.abs() as u64 + self.y.abs() as u64
    }

    pub fn navigate(&mut self, instruction: Instruction) {
        match instruction {
            Instruction::N(by) => self.waypoint.y += by,
            Instruction::S(by) => self.waypoint.y -= by,
            Instruction::E(by) => self.waypoint.x += by,
            Instruction::W(by) => self.waypoint.x -= by,
            Instruction::L(by) => self.waypoint.rotate_anticlockwise(by),
            Instruction::R(by) => self.waypoint.rotate_clockwise(by),
            Instruction::F(by) => {
                self.x += by * self.waypoint.x;
                self.y += by * self.waypoint.y;
            }
        }
    }
}

fn parse_instructions(input: &str) -> Result<Vec<Instruction>> {
    let result: std::result::Result<Vec<Instruction>, ParseInstructionError> =
        input.lines().map(|line| line.parse()).collect();
    Ok(result?)
}

fn part1(instructions: &Vec<Instruction>) -> u64 {
    let mut ship = Ship1::default();
    instructions
        .iter()
        .for_each(|&instruction| ship.navigate(instruction));

    ship.distance()
}

fn part2(instructions: &Vec<Instruction>) -> u64 {
    let mut ship = Ship2::default();
    instructions
        .iter()
        .for_each(|&instruction| ship.navigate(instruction));

    ship.distance()
}
