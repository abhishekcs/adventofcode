use std::io::{self, Read, Write};

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;

    part1(&input)?;
    part2(&input)?;
    Ok(())
}

fn part1(input: &str) -> Result<()> {
    let (keys, mut valid_passports) = input
        .lines()
        .flat_map(|line| line.split(|c| c == '\n' || c == ' '))
        .fold((0, 0), |(keys, valid_passports), property| {
            if property != "" {
                let key = property.split(':').next().unwrap();
                match key {
                    "byr" | "iyr" | "eyr" | "hgt" | "hcl" | "ecl" | "pid" => {
                        (keys + 1, valid_passports)
                    }
                    _ => (keys, valid_passports),
                }
            } else {
                if keys >= 7 {
                    (0, valid_passports + 1)
                } else {
                    (0, valid_passports)
                }
            }
        });

    if keys >= 7 {
        valid_passports += 1;
    }

    writeln!(io::stdout(), "{:?}", valid_passports)?;

    Ok(())
}

fn part2(input: &str) -> Result<()> {
    let (keys, mut valid_passports) = input
        .lines()
        .flat_map(|line| line.split(|c| c == '\n' || c == ' '))
        .fold((0, 0), |(keys, valid_passports), property| {
            if property != "" {
                let split: Vec<&str> = property.split(':').collect();
                let key = split[0];
                let value = split[1];

                if is_valid(key, value) {
                    (keys + 1, valid_passports)
                } else {
                    (keys, valid_passports)
                }
            } else {
                if keys >= 7 {
                    (0, valid_passports + 1)
                } else {
                    (0, valid_passports)
                }
            }
        });

    if keys >= 7 {
        valid_passports += 1;
    }

    writeln!(io::stdout(), "{:?}", valid_passports)?;

    Ok(())
}

fn is_valid(key: &str, value: &str) -> bool {
    match key {
        "byr" => {
            let byr: u16 = value.parse().unwrap();
            byr >= 1920 && byr <= 2002
        }
        "iyr" => {
            let iyr: u16 = value.parse().unwrap();
            iyr >= 2010 && iyr <= 2020
        }
        "eyr" => {
            let eyr: u16 = value.parse().unwrap();
            eyr >= 2020 && eyr <= 2030
        }
        "hgt" => {
            let len = value.len();
            let v = value[0..len - 2].parse();
            if !v.is_ok() {
                return false;
            }
            let hgt: u16 = v.unwrap();
            match &value[len - 2..] {
                "cm" => hgt >= 150 && hgt <= 193,
                "in" => hgt >= 59 && hgt <= 76,
                _ => false,
            }
        }
        "hcl" => {
            value.len() == 7
                && value[0..1].chars().next().unwrap() == '#'
                && value[1..].chars().all(|c| char::is_digit(c, 16))
        }
        "ecl" => {
            value == "amb"
                || value == "blu"
                || value == "brn"
                || value == "gry"
                || value == "grn"
                || value == "hzl"
                || value == "oth"
        }
        "pid" => value.len() == 9 && value.chars().all(|c| char::is_digit(c, 10)),
        _ => false,
    }
}
