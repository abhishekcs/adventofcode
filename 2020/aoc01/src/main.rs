use std::collections::{HashMap, HashSet};
use std::io::{self, Read, Write};

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;

    part1(&input)?;
    part2(&input)?;
    Ok(())
}

fn part1(input: &str) -> Result<()> {
    let mut differences = HashSet::new();
    for line in input.lines() {
        let value: i32 = line.parse()?;
        let difference = 2020 - value;
        if differences.contains(&value) {
            let product = difference * value;
            writeln!(io::stdout(), "{}", product)?;
            return Ok(());
        }

        differences.insert(difference);
    }

    panic!("No solution");
}

fn part2(input: &str) -> Result<()> {
    let mut pairs = HashMap::new();
    for (i, line) in input.lines().enumerate() {
        let value1: i32 = line.parse()?;
        for (j, line) in input.lines().enumerate() {
            if i != j {
                let value2: i32 = line.parse()?;
                pairs.insert(value1 + value2, (i, j, value1 * value2));
            }
        }
    }

    for (k, line) in input.lines().enumerate() {
        let value: i32 = line.parse()?;
        let difference = 2020 - value;
        if let Some((i, j, prod_2)) = pairs.get(&difference) {
            if k != *i && k != *j {
                let product = *prod_2 * value;
                writeln!(io::stdout(), "{}", product)?;
                return Ok(());
            }
        }
    }

    panic!("No solution");
}
