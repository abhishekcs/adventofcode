use std::io::{self, Read, Write};

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;

    part1(&input)?;
    part2(&input)?;

    Ok(())
}

fn part1(input: &str) -> Result<()> {
    let mut valid = 0;
    for line in input.lines() {
        let (password, letter, min, max) = parse_line(line)?;
        let count = password.chars().filter(|c| *c == letter).count();
        if count >= min && count <= max {
            valid += 1;
        }
    }

    writeln!(io::stdout(), "{}", valid)?;
    Ok(())
}

fn part2(input: &str) -> Result<()> {
    let mut valid = 0;
    for line in input.lines() {
        let (password, letter, idx1, idx2) = parse_line(line)?;
        let idx1 = idx1 - 1;
        let idx2 = idx2 - 1;
        let password: Vec<char> = password.chars().collect();
        if (password[idx1] != password[idx2]) && (password[idx1] == letter || password[idx2] == letter) {
            valid += 1;
        }
    }

    writeln!(io::stdout(), "{}", valid)?;
    Ok(())
}

fn parse_line(line: &str) -> Result<(&str, char, usize, usize)> {
    let idx_dash = line.find('-').expect("line does not contain '-' after minimum");
    let min: usize = line[0..idx_dash].parse()?;

    let idx_space = line.find(' ').expect("line does not contain ' ' after maximum");
    let max: usize = line[idx_dash+1..idx_space].parse()?;

    let letter = line[idx_space + 1..idx_space + 2].chars().next().unwrap();
    let password = &line[idx_space + 4..];
    Ok((password, letter, min, max))
}
