use std::collections::VecDeque;
use std::io::{self, Read, Write};

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;

    let invalid = part1(&input)?;
    part2(&input, invalid)?;
    Ok(())
}

fn part1(input: &str) -> Result<u64> {
    let mut numbers = input.lines().map(|line| -> u64 { line.parse().unwrap() });
    let mut current: VecDeque<u64> = numbers.by_ref().take(25).collect();
    let invalid = numbers
        .find(|next| {
            if !is_valid(&current, *next) {
                return true;
            }
            current.pop_front();
            current.push_back(*next);
            false
        })
        .expect("no invalid element found");

    writeln!(io::stdout(), "{}", invalid)?;
    Ok(invalid)
}

fn part2(input: &str, target: u64) -> Result<()> {
    let numbers = input.lines().map(|line| -> u64 { line.parse().unwrap() });
    let mut current: VecDeque<u64> = VecDeque::new();
    let mut sum = 0;
    for value in numbers {
        if sum + value <= target {
            sum += value;
            current.push_back(value);
            continue;
        } else if value > target {
            sum = 0;
            current.clear();
            continue;
        }

        if sum == target && current.len() > 1 {
            break;
        }

        // at this point sum + value > target so we need to drop elements from front
        sum += value;
        current.push_back(value);
        while sum > target {
            sum -= current.pop_front().unwrap();
        }
    }

    let weakness = current.iter().max().unwrap() + current.iter().min().unwrap();
    writeln!(io::stdout(), "{}", weakness)?;

    Ok(())
}

fn is_valid(current: &VecDeque<u64>, next: u64) -> bool {
    for (i, value1) in current.iter().enumerate() {
        for (j, value2) in current.iter().enumerate() {
            if i != j && (value1 + value2) == next {
                return true;
            }
        }
    }

    return false;
}
