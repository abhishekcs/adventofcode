use std::io::{self, Read, Write};
use std::{collections::HashSet, num::ParseIntError, str::FromStr};
use thiserror::Error;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

#[derive(Debug, Clone)]
pub enum Instruction {
    Nop(i64),
    Acc(i64),
    Jmp(i64),
}

#[derive(Error, Debug)]
pub enum ParseInstructionError {
    #[error("invalid instruction operation: {0}")]
    InvalidOperation(String),

    #[error("invalid instruction argument: {0}")]
    InvalidArgument(#[from] ParseIntError),
}

impl FromStr for Instruction {
    type Err = ParseInstructionError;

    fn from_str(instr: &str) -> std::result::Result<Self, Self::Err> {
        let arg: i64 = instr[4..].parse()?;
        match &instr[0..3] {
            "nop" => Ok(Instruction::Nop(arg)),
            "acc" => Ok(Instruction::Acc(arg)),
            "jmp" => Ok(Instruction::Jmp(arg)),
            _ => Err(ParseInstructionError::InvalidOperation(
                instr[0..3].to_string(),
            )),
        }
    }
}

#[derive(Debug)]
pub struct Machine {
    program_counter: usize,
    accumulator: i64,
}

impl Machine {
    pub fn run(program: Vec<Instruction>) -> std::result::Result<i64, i64> {
        let mut machine = Machine::new();
        let mut executed = HashSet::new();

        loop {
            if machine.program_counter == program.len() {
                return Ok(machine.accumulator);
            }
            machine.execute(&program[machine.program_counter]);
            if executed.contains(&machine.program_counter) {
                return Err(machine.accumulator);
            }

            executed.insert(machine.program_counter);
        }
    }

    fn new() -> Self {
        Machine {
            program_counter: 0,
            accumulator: 0,
        }
    }

    fn execute(&mut self, instr: &Instruction) {
        match instr {
            Instruction::Nop(_) => {
                self.program_counter += 1;
            }
            Instruction::Acc(val) => {
                self.accumulator += val;
                self.program_counter += 1;
            }
            Instruction::Jmp(offset) => {
                if *offset < 0 {
                    self.program_counter -= offset.abs() as usize;
                } else {
                    self.program_counter += offset.abs() as usize;
                }
            }
        }
    }
}

fn main() -> Result<()> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;

    part1(&input)?;
    part2(&input)?;

    Ok(())
}

fn part1(input: &str) -> Result<()> {
    let program = parse_program(input)?;
    match Machine::run(program) {
        Ok(_) => writeln!(io::stdout(), "Program unexpectedly terminated normally")?,
        Err(value) => writeln!(io::stdout(), "{}", value)?,
    }

    Ok(())
}

fn part2(input: &str) -> Result<()> {
    let program = parse_program(input)?;

    for i in 0..program.len() {
        let mut program = program.clone();
        match program[i] {
            Instruction::Nop(arg) => program[i] = Instruction::Jmp(arg),
            Instruction::Jmp(offset) => program[i] = Instruction::Nop(offset),
            _ => continue,
        }

        match Machine::run(program) {
            Ok(value) => {
                writeln!(io::stdout(), "{}", value)?;
                return Ok(());
            }
            Err(_) => (),
        }
    }

    writeln!(
        io::stdout(),
        "No modification enables program to terminate successfully"
    )?;
    Ok(())
}

fn parse_program(input: &str) -> std::result::Result<Vec<Instruction>, ParseInstructionError> {
    input.lines().map(|line| line.parse()).collect()
}
