use std::io::{self, Read, Write};

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;

    part1(&input)?;
    part2(&input)?;
    Ok(())
}

fn part1(input: &str) -> Result<()> {
    solve(input, 0, |a, b| a | b)
}

fn part2(input: &str) -> Result<()> {
    solve(input, u32::MAX, |a, b| a & b)
}

fn solve<F>(input: &str, init: u32, op: F) -> Result<()>
where
    F: Fn(u32, u32) -> u32,
{
    let (total, _): (u32, u32) =
        input
            .lines()
            .chain(vec![""].into_iter())
            .fold((0, init), |(total, current), line| {
                if line.len() == 0 {
                    (total + current.count_ones(), init)
                } else {
                    (total, op(current, count(line)))
                }
            });

    writeln!(io::stdout(), "{}", total)?;
    Ok(())
}

fn count(line: &str) -> u32 {
    line.bytes()
        .fold(0, |acc, b| acc | 2u32.pow((b - b'a') as u32))
}
