use std::io::{self, Read, Write};

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

fn main() -> Result<()> {
    let mut input = String::new();
    io::stdin().read_to_string(&mut input)?;

    part1(&input)?;
    part2(&input)?;
    Ok(())
}

fn part1(input: &str) -> Result<()> {
    writeln!(io::stdout(), "{}", traverse_slopes(input, 3, 1).unwrap())?;
    Ok(())
}

fn part2(input: &str) -> Result<()> {
    let solution = traverse_slopes(input, 1, 1).unwrap() 
        * traverse_slopes(input, 3, 1).unwrap()
        * traverse_slopes(input, 5, 1).unwrap()
        * traverse_slopes(input, 7, 1).unwrap()
        * traverse_slopes(input, 1, 2).unwrap();
    
    writeln!(io::stdout(), "{}", solution)?;
    Ok(())
}

fn traverse_slopes(input: &str, right: usize, down: usize) -> Result<usize> {
    let mut i = 0;
    let mut j = 0;
    let mut trees = 0;
    let lines: Vec<&str> = input.lines().collect();
    while (i + down) < lines.len() {
        i += down;
        let line: Vec<char> = lines[i].chars().collect();
        j = (j + right) % lines[i].len();
        if line[j] == '#' {
            trees += 1;
        }
    }

    Ok(trees)
}
